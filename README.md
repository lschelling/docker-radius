# RADIUS Container

Basic Docker container for FreeRADIUS.

The purpose of this RADIUS configuration is for development and testing only. It's not intended to run this RADIUS server in a productive environment.

### Checkout from bitbucket
~~~~
git clone https://lschelling@bitbucket.org/lschelling/docker-radius.git
~~~~

### Build the image with
~~~~
cd docker-radius
sudo docker build -t="user/radius" .
~~~~

### Create the container
~~~~
sudo docker run  -p 1812:1812/udp -t -i user/radius --name my_radius_container
~~~~

### Usage
The container contains 2 predefined users:

* admin:password (with Service-Type Adminstrative-User)
* maint:password
* vadim:password (with Service-Type Adminstrative-User and Vendor Specific attribute)
* vmaint:password (with Vendor Specific attribute)

The FreeRADIUS config will allow anyone to access it with the secret`testing123`.

### Change user database
The start.sh script will also take user details from env and add them as users.
Eg running this container with `-e RADIUS_USER_test=password` will add a user
with the username "test" and the password "password".

The /etc/freeradius directory is exposed as a volume, and FreeRADIUS will
reload if it receives a SIGHUP. In combination this means you can modify files
in the volume, and then `pkill -HUP freeradius` (approximate, tweak as needed)
to make more enthusiastic modifications to the FreeRADIUS configuration.
